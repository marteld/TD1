<?php

namespace TheFeed\Controleur;


use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\MessageFlash;
use TheFeed\Modele\DataObject\Publication;
use TheFeed\Modele\Repository\PublicationRepository;
use TheFeed\Modele\Repository\UtilisateurRepository;

use Symfony\Component\HttpFoundation\Response;
class ControleurPublication extends ControleurGenerique
{

    public static function afficherListe(): Response
    {
        $publications = (new PublicationRepository())->recuperer();
        return ControleurUtilisateur::afficherVue('base.html.twig', [
            "publications" => $publications,
            "pagetitle" => "The Feed",
            "cheminVueBody" => "publication/liste.php"
        ]);
    }

    public static function creerDepuisFormulaire()
    {
        $idUtilisateurConnecte = ConnexionUtilisateur::getIdUtilisateurConnecte();
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($idUtilisateurConnecte);

        if ($utilisateur == null) {
            MessageFlash::ajouter("error", "Il faut être connecté pour publier un feed");
            return ControleurPublication::rediriger('connecter');
        }
        $message = $_POST['message'];
        if ($message == null || $message == "") {
            MessageFlash::ajouter("error", "Le message ne peut pas être vide!");
            return ControleurPublication::rediriger('afficherListe');
        }
        if (strlen($message) > 250) {
            MessageFlash::ajouter("error", "Le message ne peut pas dépasser 250 caractères!");
            return ControleurPublication::rediriger('afficherListe');
        }

        $publication = Publication::create($message, $utilisateur);
        (new PublicationRepository())->ajouter($publication);

        return ControleurPublication::rediriger('afficherListe');
    }


}